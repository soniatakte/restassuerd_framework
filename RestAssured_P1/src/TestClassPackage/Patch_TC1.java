package TestClassPackage;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Test;

import CommonMethodpackage.Trigger_API_Method;

import CommonMethodpackage.Trigger_Put_API_Method;
import CommonUtilityPackage.Handle_Logs;
import RequestRepository.Patch_RequestRepository;

import io.restassured.path.json.JsonPath;

public class Patch_TC1 extends Patch_RequestRepository{
	@Test
	public static void executor() throws IOException {
		File dirname=Handle_Logs.Create_Log_Directory("Patch_TC1");//Evidence
		
		String RequestBody = Patch_TC1_Request();

		int StatusCode = Trigger_Put_API_Method.extract_Status_Code(RequestBody, patch_endpoint());
		System.out.println(StatusCode);

		String ResponseBody = Trigger_API_Method.extract_Response_Body(RequestBody, patch_endpoint());
		System.out.println(ResponseBody);
		Handle_Logs.Evidence_Creator(dirname, "Patch_TC1", patch_endpoint() , Patch_TC1_Request(), ResponseBody);
		
		validator(RequestBody,ResponseBody);
	}

	public static void validator(String RequestBody , String ResponseBody) {

		// Create an object for requestbody
		JsonPath jsp_req = new JsonPath(RequestBody);

		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		// Create an object for ResponseBody
		JsonPath jsp_res = new JsonPath(ResponseBody);

		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");

		// Validation
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);

	}
}
