package TestClassPackage;

import RequestRepository.Post_RequestRepository;
import io.restassured.path.json.JsonPath;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.Test;

import CommonMethodpackage.Trigger_API_Method;
import CommonUtilityPackage.Handle_Logs;

public class Post_TC1 extends Post_RequestRepository {
	@Test
	public static void executor() throws IOException {
		File dirname = Handle_Logs.Create_Log_Directory("Post_TC1");// Evidence

		String RequestBody = Post_TC1_Request();

		for (int i = 0; i < 5; i++) {
			int StatusCode = Trigger_API_Method.extract_Status_Code(RequestBody, post_endpoint());

			// System.out.println(StatusCode);
			if (StatusCode == 201) {

				String ResponseBody = Trigger_API_Method.extract_Response_Body(RequestBody, post_endpoint());
				// System.out.println(ResponseBody);
				// Handle_Logs.Evidence_Creator(dirname, "Post_TC1", post_endpoint() ,
				// Post_TC1_Request(), ResponseBody);

				validator(RequestBody, ResponseBody);
				break;
			} else {
				System.out.println("Desired Statuscode not found hence, retry");
			}
		}
	}

	public static void validator(String RequestBody, String ResponseBody) throws IOException {

		// Create an object for requestbody
		JsonPath jsp_req = new JsonPath(RequestBody);

		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		LocalDateTime CurrentDate = LocalDateTime.now();
		String ExpectedDate = CurrentDate.toString().substring(0, 11);

		// Create an object for ResponseBody
		JsonPath jsp_res = new JsonPath(ResponseBody);

		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		String res_createdAt = jsp_res.getString("createdAt").substring(0, 11);

		// Validation
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(ExpectedDate, res_createdAt);

	}

}
