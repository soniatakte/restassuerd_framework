package Oops_concept;

//for Multilevel Inheritance create Sub Class2

/*public class Javadev extends CSStudent {
	
	//Method
	public void project() {
		System.out.println(name+ " is developing application in Java");
	}

	public static void main(String[] args) {
		
		//create an object for Sub Class2
		Javadev jvd=new Javadev();
		
		//call the methods from super class nd Sub classes
		
		jvd.name="Tapasya";
		jvd.rollno=10;
		jvd.study();  //method from superclass
		jvd.code();		//method from subclass1
		jvd.project();  //method frm subclass2
	
			
	}
}*/

	public class Javadev extends CSStudent {
		
		public void project() {
			System.out.println("she is developing application in java");
			
		}
		
		public static void main (String [] arg) {
			
			Javadev jv = new Javadev ();
			
			jv.name="Tapasya";
			jv.study();
			jv.code();
			jv.project();
			
					
		}
	}


