package Oops_concept;

//Method OVERLODING

public class Fan {
	
	//Method 
	public void setSpeed() {
		System.out.println("Fan speed set to LOW");
	}
	
	//Method OVERLOADING 
	public void setSpeed(int speed) {
		System.out.println("Fan speed set to MEDIUM");
	}
	
	//Method OVERLOADING
	public void setSpeed (int speed , String unit) {
		//System.out.println("Fan speed set to " +speed + "  " +unit);
		System.out.println("Fan speed set to High");
	}
	
	
	public static void main(String[] args) {
		
		//create an object for class fan
		
		Fan fs=new Fan();
		
		fs.setSpeed();
		fs.setSpeed(2);
		fs.setSpeed(3,"HIGH");
	

	}

}
