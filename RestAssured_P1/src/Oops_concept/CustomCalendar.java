package Oops_concept;

public class CustomCalendar extends Calendar{

	//Method OVERRIDING
	public void getDate() {
		System.out.println("September 4 ,2023");
	}
	
	public static void main(String[] args) {
		
		//Create an object
		
		CustomCalendar cal=new CustomCalendar(); 
		
		//call the method
		
		cal.getDate();
		
	}
}


