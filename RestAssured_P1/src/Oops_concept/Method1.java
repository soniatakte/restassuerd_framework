package Oops_concept;

public class Method1 {
	
	
    
	//method Overloading using diff type of Arguments Sting & int
	public void displayinfo(String name) {
		System.out.println( "Student Name:" +name);
		
		
	}
	public void displayinfo(int rollno) {
		System.out.println("Student Rollno :" +rollno);
	}
	
	public void displayinfo(String subject , int result) {
	System.out.println("Subject name : " + subject+ " and result in % : " +result);
	}

	public static void main(String[] args) {
	
     //create an object 
		 
		Method1 nm=new Method1();
		
		//call the Overload method
		//nm.displayinfo(" Raghav");
		//nm.displayinfo( 01);
	    nm.displayinfo("Science",75 );
		
		
	}

}
