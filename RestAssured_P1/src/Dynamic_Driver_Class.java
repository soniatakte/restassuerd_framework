import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import CommonUtilityPackage.Excel_Data_Reader;

public class Dynamic_Driver_Class {

	public static void main(String[] args)
			throws IOException, ClassNotFoundException, NoSuchMethodException, SecurityException,
			InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {

		// Step1 : Read the test cases to be executed from excel file

		ArrayList<String> TestCaseList = Excel_Data_Reader.Read_Excel_data("API_Data.xlsx", "TestCasesToExecute",
				"TestCaseToExecute");
		System.out.println(TestCaseList);
		int count = TestCaseList.size();

		for (int i = 1; i < count; i++) {
			String TestCaseToExecute = TestCaseList.get(i);
			System.out.println("Test case which is going to be execute :" +TestCaseToExecute);

			// Step2 : Call the TestCaseToExecute on runtime by using java.lang.reflect
			// package

			Class<?> TestClass = Class.forName("TestClassPackage." +TestCaseToExecute);

			// Step3: Call the execute method of the class captures in variable in test
			// class by using java.lang.reflect.method

			Method ExecuteMethod = TestClass.getDeclaredMethod("executor");

			// Step4 : Set the accessibility of method as true

			ExecuteMethod.setAccessible(true);

			// Step5:Create the instance of class captured in the TestClass variable

			Object InstanceOfTestClass = TestClass.getDeclaredConstructor().newInstance();

			// Step6: Execute the method captured in variable ExecuteMethod of class
			// captured inTestClass variable

			ExecuteMethod.invoke(InstanceOfTestClass);
			System.out.println("Execution of testcase name " + TestCaseToExecute + "is completed");
			System.out.println("::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");

		}
	}

}
