package CommonUtilityPackage;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Excel_Data_Reader {
	public static ArrayList<String> Read_Excel_data(String File_Name, String Sheet_Name, String Test_CaseName)
			throws IOException {

		ArrayList<String> Array_Data = new ArrayList<String>();

		// Step1: Locate the File
		String Project_Dir = System.getProperty("user.dir");
		FileInputStream fis = new FileInputStream(Project_Dir + "\\Input_Data\\" + File_Name);

		// Step2: Access the located Excel File
		XSSFWorkbook wb = new XSSFWorkbook(fis);

		// Step3: Count the number of sheets available in the Excel File
		int countofsheet = wb.getNumberOfSheets();
		System.out.println(countofsheet);

		// Step4: Access the desired sheet

		for (int i = 0; i < countofsheet; i++) {

			String sheetname = wb.getSheetName(i);
			if (sheetname.equals(Sheet_Name)) {
				System.out.println("inside the sheet : " + sheetname);
				XSSFSheet sheet = wb.getSheetAt(i);
				Iterator<Row> Rows = sheet.iterator(); // due to iterator method we can use hasnext
				while (Rows.hasNext()) {
					Row currentRow = Rows.next(); // It will return next row to me

					// Step5: Access the Row corresponding to desired testcase
					String cellvalue = currentRow.getCell(0).getStringCellValue();
					if (currentRow.getCell(0).getStringCellValue().equals(Test_CaseName)) {
						Iterator<Cell> Cell = currentRow.iterator();
						while (Cell.hasNext()) {
							String data = Cell.next().getStringCellValue();
							System.out.println(data);
							Array_Data.add(data);
						}

					}

				}

			}

		}
		wb.close();
		return Array_Data;

	}

}
