package Abstraction_Concept;

public class TestBank {

	public static void main(String[] args) {
		
		//Create an object for SBIbank
		
		SBIbank sb=new SBIbank();
		
		sb.deposit(); //parentclass
		sb.withdraw(); //parentclass
		sb.loan();    // overriden frm bank
		sb.credit(); //non overriden only available for SBI bank
	}

}
