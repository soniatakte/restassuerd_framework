package Abstraction_Concept;

public abstract class Bank {
	
	public abstract void loan(); //abstract method - no method body*
	
	
	//Non abstract method 
	public void deposit() {
		System.out.println("Bank ---- Deposit Amt");
	}
	
	public void withdraw() {
		System.out.println("Bank ------ Withdraw Amt");
	}
		
	}


