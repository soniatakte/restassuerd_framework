package String_Concept;

public class Duplicate_Char {

	public static void main(String[] args) {
		
		String str="accessibility";
		int count;
		
		//convert given string into character array
		char string[]=str.toCharArray();
		System.out.println("Duplicate character in string:");
		
		//count each character present in the string
		for (int i=0; i<string.length;i++) {
			count=1;
			for(int j=i+1;j<string.length;j++) {
				if(string[i] == string[j] && string[i]!='0') {
					count++;
					string[j]=0;
				}
			}
			if(count>1 && string[i]!= '0') 
				System.out.println(string[i]);
				
		}
	}

}
