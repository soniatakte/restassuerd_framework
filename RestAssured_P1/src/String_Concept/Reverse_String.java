package String_Concept;

public class Reverse_String {

	public static void main(String[] args) {
		String str = "sonia";
		String rev = "";

		for (int i = 0; i < str.length(); i++) {

			rev = str.charAt(i) + rev;

		}
		System.out.println("Reversed String : " + rev);
	}
}
