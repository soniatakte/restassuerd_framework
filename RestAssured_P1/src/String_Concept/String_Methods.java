package String_Concept;

import org.codehaus.groovy.ast.stmt.AssertStatement;

public class String_Methods {

	public static void main(String[] args) {
		String st = "hello world";
		System.out.println("Find Length:" + st.length());// to find the length of the String //27

		System.out.println("find Index : " + st.charAt(6));// To find the INDEX of the string
		//compare
		String st1 = "hello world";
		System.out.println("comparison: " + st.compareTo(st1));// COMPARES two String : reurn 0 coz they're equal

		String st2 = "HELLO WORLD";
		System.out.println("Ignore Case" + st.compareToIgnoreCase(st2)); // Ingnore Case

		System.out.println("Comapare with TRUE/False result : " + st.equals(st1));

		// Conacat
		String fn = "Sonia";
		String ln = "Patil";
		System.out.println("concat two string : " + fn.concat(ln));

		// contains a charachter
		System.out.println("Dose String contain define alphabet : " + fn.contains("i"));// True
		System.out.println(ln.contains("n"));

		// copy vaule :copyValueOf
		char[] one = { 's', 'o', 'n', 'a' };
		String two = "";
		two = String.copyValueOf(one, 0, 4);
		System.out.println("Return String : " + two);

		// Check the string ends with specified char
		System.out.println("Check the string ends with specified char: " + st1.endsWith("d"));// True boolean ans

		// REPLACE
		System.out.println("replace string :" + st1.replace('o', 'a'));
		
		//REVERSE
		

	}

}
