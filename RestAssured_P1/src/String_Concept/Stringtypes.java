package String_Concept;

public class Stringtypes {

	public static void main(String[] args) {
		String s1 = "sonia";
		String s2 = new String("sonia");
		String s3 = "sonia";
		String s4 = new String("sonia");

		System.out.println("1st case" + s1.equals(s3)); // True
		// Equals method compare values
		System.out.println("2nd case " + s1.equals(s2));// True

		System.out.println("3rd case" + s1 == s2);// false
		// equal to equal compares references
		System.out.println("4th case" + s1 == s3);// false

	}

}
