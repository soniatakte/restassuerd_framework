package restAssuredReferenc;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;

import org.testng.Assert;
public class PutReference {

	public static void main(String[] args) {
		//Declare the variables for BaseURI and Request Body
		String BaseURI="https://reqres.in/";
		String RequestBody="{\r\n"
				+ "    \"name\": \"morpheus\",\r\n"
				+ "    \"job\": \"zion resident\"\r\n"
				+ "}";
		
		//Declare BaseURI
		
		RestAssured.baseURI=BaseURI;
		
		//Step 3: Configure Request Body and Triggre the API
		
		/*String responsebody=given().header("Content-Type","application/json").body(RequestBody).
				  when().post("api/users")
				.then().extract().response().asString();
				*/
		
		String ResponseBody=given().header("Content-Type","application/json").body(RequestBody).
		when().put("api/users/2").then().extract().response().asString();
		System.out.println(ResponseBody);
		
		// create an object JsonPath to parse the RequestBody
		
		JsonPath jsp_req=new JsonPath(RequestBody);
		
		String req_name=jsp_req.getString("name");
		System.out.println("RequestBody Name:"+req_name);
		
		String req_job=jsp_req.getString("job");
		System.out.println("RequestBody Job:"+req_job);
		
		/*String req_updatedAt=jsp_req.getString("updatedAt");
		System.out.println("RequestBody"+req_updatedAt);*/
		
		//create an object JsonPath to parse the ResponseBody
		
		JsonPath jsp_res=new JsonPath(ResponseBody);
		
		String res_name=jsp_res.getString("name");
		System.out.println("ResponseBody name"+res_name);
		
		String res_job=jsp_res.getString("job");
		System.out.println("ResponseBody Job"+res_job);
		
		String res_updatedAt=jsp_res.getString("updatedAt");
		System.out.println("ResponseBody updatedAt"+res_updatedAt);
		
		//Validation
		
		Assert.assertEquals(res_name,req_name);
		Assert.assertEquals(res_job,req_job);
		Assert.assertNotNull(res_updatedAt);
		

	}

}
