package restAssuredReferenc;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;

import org.testng.Assert;

public class Post_Reference1 {
	
	
	public static void main(String[] args) {
		String BaseURI="//reqres.in/";
		String RequestBody="{\r\n"
				+ "    \"name\": \"morpheus\",\r\n"
				+ "    \"job\": \"leader\"\r\n"
				+ "}";
		
		RestAssured.baseURI=BaseURI;
		
		String responsebody=given().header("Content-Type","application/json").body(RequestBody)
				.when().post("api/users/2")
				.then().extract().response().asString();
		System.out.println(responsebody);

		//create an json object
		JsonPath Jsp_req=new JsonPath(RequestBody);
		
		String req_name=Jsp_req.getString("name");
		System.out.println(req_name);
		
		String req_job=Jsp_req.getString("job");
		System.out.println(req_job);
		
		String req_id=Jsp_req.getString("id");
		System.out.println(req_id);
		
		String req_createdAt=Jsp_req.getString("createAt");
		System.out.println(req_createdAt);
		
		JsonPath Jsp_res=new JsonPath(responsebody);
		
		String res_name=Jsp_res.getString("name");
		System.out.println(res_name);
		
		String res_job=Jsp_res.getString("job");
		System.out.println(res_job);
		
		String res_id=Jsp_res.getString("id");
		System.out.println(res_id);
		
		String res_createdAt=Jsp_res.getString("createdAt");
		System.out.println("ResponseBody:"+res_createdAt);
		
		Assert.assertEquals(res_name,req_name);
		
		
		
		
		
	}

}
