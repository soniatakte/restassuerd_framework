package restAssuredReferenc;

import io.restassured.RestAssured;
import static io.restassured.RestAssured.given;
public class GetReference {

	public static void main(String[] args) {
		//Step 1: Declare the variables for the base URI
		String BaseURI="https://reqres.in/";
		
		//Step 2: Declare BaseURI
		RestAssured.baseURI=BaseURI;
		
		//Step 3: Configure RequestBody and Trigger the API
		
		// Without using log().all().Method
				
		/*String responsebody=given().header("Content-Type","application/json").body(RequestBody).
		  when().post("api/users")
		 .then().extract().response().asString();
		  System.out.println(responsebody);*/
		
		String ResponseBody=given().
		when().get("api/users/2").then().extract().response().asString();
		System.out.println(ResponseBody);
		

	}

}
