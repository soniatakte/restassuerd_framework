package restAssuredReferenc;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import static io.restassured.RestAssured.given;

import java.time.LocalDateTime;

import org.testng.Assert;
public class PostReference {

	public static void main(String[] args) {
	//Step 1 : Declare the variable for base URI and Request Body 
		String BaseURI="https://reqres.in/";
		String RequestBody="{\r\n"
				+ "    \"name\": \"morpheus\",\r\n"
				+ "    \"job\": \"leader\"\r\n"
				+ "}";
		//Step 2: BaseURI
		RestAssured.baseURI=BaseURI;
		
		// Step 3:Configure request body and trigger the API
		
		// Method 1: Without using log().all(). 
		//will get the response in suppressed format i.e.only Responsebody will be printed, parameters,Headers & other
		//info won't be printed on console.
		
		String responsebody=given().header("Content-Type","application/json").body(RequestBody).
		when().post("api/users")
	    .then().extract().response().asString();
		System.out.println(responsebody);
		
		// Method 2: Using log().all().method-
		//will get the response in full format i.e.along with responsebody para,headers & other info will print on console
		
	     /*given().headers("Content-Type","application/json").body(RequestBody).log().all()
		.when().post("api/users")).log().all().extract().response().asString();*/
		
		// Step 4: Create Object of JsonPath and parse the Request Body 
		
		JsonPath jsp_req=new JsonPath(RequestBody);
		
		
		String req_name=jsp_req.getString("name");
		System.out.println("Requestbody Name:"+req_name);
		
		String req_job=jsp_req.getString("job");
		System.out.println("RequestBody Job: "+req_job);
				
						
		//Step 5: Create Object of JsonPath and parse the ResponseBody
		JsonPath jsp_res=new JsonPath(responsebody);
		
		String res_name=jsp_res.getString("name");
		System.out.println("ResponseBody Name:"+res_name);
		
		String res_job=jsp_res.getString("job");
		System.out.println("ResponseBody Job:" +res_job);
		
		String res_id=jsp_res.getString("id");
		System.out.println("ResponseBody id:" +res_id);
		

		String res_createdAt=jsp_res.getString("createdAt");
		System.out.println("ResponseBody createdAt" +res_createdAt);
		String mydate=res_createdAt.substring(0,10);
		
		//Fetch Local System Date
		LocalDateTime CurrentDate=LocalDateTime.now();
		System.out.println(CurrentDate);
		String ExpectedDate=CurrentDate.toString().substring(0,10);
		System.out.println("This is createdAt date:"+mydate);
		System.out.println("This is an ExpectedDate"+ExpectedDate);
		
					
		//step 6: Validate the response body parameters
		
		Assert.assertEquals(res_name,req_name);
		Assert.assertEquals(res_job,req_job);
		Assert.assertNotNull(res_id);
		Assert.assertEquals(mydate,ExpectedDate);
		
		
		
	

	}

}
