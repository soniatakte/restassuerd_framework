package restAssuredReferenc;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;

import org.testng.Assert;
public class PatchReference {

	public static void main(String[] args) {
		//Step 1: Declare the variables for Basic URI and Requestbody
		
		String BaseURI="https://reqres.in/";
		String RequestBody="{\r\n"
				+ "    \"name\": \"morpheus\",\r\n"
				+ "    \"job\": \"zion resident\"\r\n"
				+ "}";
		
		//Step2: Declare BaseURI
		RestAssured.baseURI=BaseURI;
		
		//Step 3: Configure the requestbody and trigger the API
		
		String ResponseBody=given().header("Content-Type","application/json").body(RequestBody).
		when().patch("api/users/2").then().extract().response().asString();
		System.out.println(ResponseBody);
		
		//create an Object JsonPath to parse the RequestBody
		JsonPath jsp_req=new JsonPath(RequestBody);
		
		String req_name=jsp_req.getString("name");
		System.out.println("Requestbody Name:"+req_name );
		
		String req_job=jsp_req.getString("job");
		System.out.println("Requestbody Job:" +req_job);
		
		//create an Object JsonPath to parse the Responsebody
		
		JsonPath jsp_res=new JsonPath(ResponseBody);
		
		String res_name=jsp_res.getString("name");
		System.out.println("ResponseBody Name:" +res_name);
		
		String res_job=jsp_res.getString("job");
		System.out.println("ResponseBody Job:" +res_job);
		
		String res_updatedAt=jsp_res.getString("updatedAt");
		System.out.println("ResponseBody UpdatedAt:"+res_updatedAt);
		
		//Validation
		
		Assert.assertEquals(res_name,res_name);
		Assert.assertEquals(res_job, req_job);
		
		
		

	}

}
