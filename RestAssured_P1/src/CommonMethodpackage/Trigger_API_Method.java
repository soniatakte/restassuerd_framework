package CommonMethodpackage;

import static io.restassured.RestAssured.given;

import RequestRepository.Endpoints;

public class Trigger_API_Method extends Endpoints{
		public static int extract_Status_Code(String req_Body,String URL) {
			int StatusCode =given().header("Content-Type","application/json")
					.body(req_Body)
					.when().post(URL)
					.then().extract().statusCode();
					return StatusCode;
		}
				//Extract Responsebody
		
			public static String extract_Response_Body(String req_Body,String URL) {
				String ResponseBody=given().header("Content-Type","application/json")
						.body(req_Body)
						.when().post(URL)
						.then().extract().response().asString();
				
				return ResponseBody;
				
				
			}
	}
		

