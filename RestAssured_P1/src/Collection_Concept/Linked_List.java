package Collection_Concept;

import java.util.Iterator;
import java.util.LinkedList;

public class Linked_List {

	public static void main(String[] args) {

		// Declare Linked List for HETROGENOUS DATA
		// LinkedList<Integer> l = new LinkedList<Integer> (); //For homogenousData
		// LinkedList<String> l = new LinkedList<String>();

		LinkedList l = new LinkedList();

		// Add elements into LinkedLIst
		l.add(100);
		l.add("welcome");
		l.add(15.5);
		l.add('A');
		l.add(true);
		l.add(null);

		System.out.println(l); // [100, welcome, 15.5, A, true, null]
		System.out.println(l.size()); // 6

		// remove
		l.remove(3);
		System.out.println("After removing the element :" + l); // [100, welcome, 15.5, true, null]

		// add element
		l.add(3, "sonia");
		System.out.println("After adding the element : " + l);// [100, welcome, 15.5, sonia, true, null]

		// retrieve the value
		System.out.println(l.get(4)); // true

		// Change the value
		l.set(5, 'x');
		System.out.println("After changing the value :" + l); // [100, welcome, 15.5, sonia, true, x]

		// contais()
		System.out.println(l.contains("sonia")); // true
		System.out.println(l.contains(5)); // false

		// osEmpty()
		System.out.println(l.isEmpty()); // false

		// read element using for loop

	/*	for (int i = 0; i < l.size(); i++) {

			System.out.println(l.get(i));
		} */
		
		//iterator() method
		
		Iterator it = l.iterator();
		while(it.hasNext())
		{
			System.out.println(it.next());
		}
		
	}
}
