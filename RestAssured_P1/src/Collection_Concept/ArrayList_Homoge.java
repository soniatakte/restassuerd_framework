package Collection_Concept;

import java.util.ArrayList;
import java.util.Collections;

public class ArrayList_Homoge {

	public static void main(String[] args) {

		ArrayList al = new ArrayList();

		al.add('m');
		al.add('n');
		al.add('c');
		al.add('d');
		al.add('e');

		ArrayList al_dup = new ArrayList();

		al_dup.addAll(al);
		System.out.println(al_dup); // 

		//RemoveAll
		al_dup.removeAll(al);  //[m, n, c, d, e]
		System.out.println("After removing :" +al_dup); //After removing :[]
		
		//Sort......collections.sort
		System.out.println("Elements in the ArrayList :" +al);
		Collections.sort(al);
		System.out.println("Elements in the arraylist after sorting :" +al); //[c, d, e, m, n]
		
		//Sort....in Reverse order
		Collections.sort(al,Collections.reverseOrder());
		System.out.println("Elements after sorting in reverse order :" +al); //[n, m, e, d, c]
		
		//Shuffling.....collections.shuffle
		Collections.shuffle(al);
		System.out.println("Elements after shuffling :" +al); //[n, c, m, d, e]

		
		
		
	}

}
