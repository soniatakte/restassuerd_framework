package Collection_Concept;

import java.util.ArrayList;

public class ArrayList_methods {

	public static void main(String[] args) {
		// Declare ArrayList for hetrogenous
		ArrayList<Object> al = new ArrayList<>();

		// 2nd way to declare ArrayList for Homogenous data
		// ArrayList <Integer> al = new ArrayList<Integer>();

		// Add new elemnets to the arraylist
		al.add(50);
		al.add("hello");
		al.add(12.3);
		al.add('B');
		al.add(true);
		
		System.out.println(al);
		
		//Size
		System.out.println("Number of elements in arraylist :" +al.size());//5
		
		//REMOVE
		al.remove(3); //here 3 is a INDEX
		System.out.println("After removing element :" +al);
		
		//Insert New element
		al.add(2,"team");
		System.out.println("After insertion : " +al);//[50, hello, team, 12.3, true]
		
		//retrive specific element
		System.out.println(al.get(2)); //team
		
		//replace the element
		al.set(4, "c++");
		System.out.println("After replacing element : " +al); //[50, hello, team, 12.3, c++]
		
		//Search
		System.out.println("contains the element :"+al.contains("sonia"));  //false
		System.out.println("contans the element :" +al.contains("c++"));	//true
		
		//isEmpty
		System.out.println(al.isEmpty());  //false
		
		//Method1:read elemnets using FOR LOOP
		
		System.out.println("retrive data using for loop......");
		for(int i=0;i<al.size();i++)
		{
			
			System.out.println(al.get(i));
		}
		
		//Method2: using for ....each loop
		/*System.out.println("retrive data using for.....each loop");
		for(Object e:al)
		{
			System.out.println(al);
		}*/
		
		//
		
		
		
		
		
		
			
		

	}

}
