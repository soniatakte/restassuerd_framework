package Collection_Concept;

import java.util.HashSet;

public class Hashset_Method2 {

	public static void main(String[] args) {

		HashSet<Integer> evennum = new HashSet<Integer>();

		evennum.add(4);
		evennum.add(6);
		evennum.add(8);

		System.out.println(evennum); // [4, 6, 8]

		HashSet<Integer> num = new HashSet<Integer>();
		// addAll
		num.addAll(evennum);
		num.add(10);
		System.out.println("New Hashset : " + num); // [4, 6, 8, 10]

		// removeAll
		num.removeAll(evennum);
		System.out.println("After removing the element " + num); // [10]

	}

}
