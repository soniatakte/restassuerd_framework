package Collection_Concept;

import java.util.LinkedList;

public class LinkedList_SpecialMethod {

	public static void main(String[] args) {

		LinkedList l = new LinkedList();

		l.add("dog");
		l.add("cat");
		l.add("dog");
		l.add("horse");

		System.out.println(l); // [dog, cat, dog, horse]

		// Stack & queue methods

		l.addFirst("Tiger");
		l.addLast("monkey");

		System.out.println(l);  //[Tiger, dog, cat, dog, horse, monkey]
		
		//return element
		
		System.out.println(l.getFirst()); //Tigher
		System.out.println(l.getLast());  //MONKEY
		
		//remove first & last element
		
		l.removeFirst();
		l.removeLast();
		System.out.println("After removing :" +l); //[dog, cat, dog, horse]
		

	}

}