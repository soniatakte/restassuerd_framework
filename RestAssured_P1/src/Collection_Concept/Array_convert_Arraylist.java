package Collection_Concept;

import java.util.ArrayList;
import java.util.Arrays;

public class Array_convert_Arraylist {

	public static void main(String[] args) {
		
		String ar[] = {"Dog","Cat","Cow"};
		for (String value:ar)
		{
			System.out.println(value);
		}
		
		ArrayList al = new ArrayList(Arrays.asList(ar));
		System.out.println(al);                             //[Dog, Cat, Cow]
		
				

	}

}
