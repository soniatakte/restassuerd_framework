package Collection_Concept;

import java.util.HashSet;
import java.util.Iterator;

public class Hashset_Methods {

	public static void main(String[] args) {

		HashSet hs = new HashSet(); // default capacity is 16 , loadFactor is 0.75

		// Hashset hs = new HashSet(100); // capacity is 100

		// HashSet hs = new HashSet(100, (float) (0.90)); //with loadfactor

		// HashSet<Integer> hs = new HashSet<Integer>(); //Homogeneous data

		// Add elements
		hs.add(5);
		hs.add(100);
		hs.add("cow");
		hs.add(null);
		hs.add(false);

		System.out.println(hs); // [null, 100, 5, false, cow] : insertion order is not preserved

		// remove
		hs.remove(5);
		System.out.println("After removing a object : " + hs); // [null, 100, false, cow]

		// contains method coz SET does not hv get, set method
		System.out.println(hs.contains(null)); // true
		System.out.println(hs.contains("dog")); // false

		// isEmpty()

		System.out.println(hs.isEmpty()); // false

		// Method 1: reading elements from hashset using for---each
		/*
		 * for(Object e:hs) { System.out.println(e);
		 */

		// Method 2: Iterator Method

		Iterator it = hs.iterator();
		while (it.hasNext()) {
			System.out.println(it.next());
		}

	}
}
