package Collection_Concept;

import java.util.Collections;
import java.util.LinkedList;

public class LinkedList_2 {

	public static void main(String[] args) {
		
		LinkedList l = new LinkedList();
		
			l.add('a');
			l.add('c');
			l.add('f');
			l.add('k');
			l.add('h');
			
			System.out.println(l);
			
			//ad group of object into new linked list
			
			LinkedList new_l = new LinkedList();
			
			new_l.addAll(l);
			System.out.println(new_l);  //[a, c, f, k, h]

			//removeAll
			
			new_l.removeAll(l);
			System.out.println(new_l); //[]
			
			//Sort using collections
			
			Collections.sort(l);
			System.out.println("After Sorting :" +l);
			
			//sorting in reverse order
			Collections.sort(l,Collections.reverseOrder()); //[k, h, f, c, a]

			System.out.println(l);
			
			//shuffling
			
			Collections.shuffle(l);
			System.out.println("After the shuffling :" +l);  //[k, a, c, f, h]
			
	}
	

}
