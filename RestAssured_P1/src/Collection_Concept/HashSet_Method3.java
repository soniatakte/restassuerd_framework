package Collection_Concept;

import java.util.HashSet;

public class HashSet_Method3 {

	public static void main(String[] args) {

		// Union , Intersection , difference

		HashSet<Integer> set1 = new HashSet<Integer>();

		set1.add(1);
		set1.add(2);
		set1.add(3);
		set1.add(4);
		set1.add(5);

		System.out.println("First HashSet  : " + set1); // [1, 2, 3, 4, 5]

		HashSet<Integer> set2 = new HashSet<Integer>();

		set2.add(3);
		set2.add(5);
		set2.add(8);

		System.out.println("Second hashset  : " + set2); // [3, 5, 8]

		// union : it will remove duplicates

		set1.addAll(set2);
		System.out.println("union : " + set1); // [1, 2, 3, 4, 5, 8]

		// intersection : give common elements

		set1.retainAll(set2);
		System.out.println("Intersection elements :" + set1); // [3, 5, 8]

		// difference

		set1.removeAll(set2);
		System.out.println("Difference : " + set1);

	}

}
