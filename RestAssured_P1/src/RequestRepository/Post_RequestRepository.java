package RequestRepository;

import java.io.IOException;
import java.util.ArrayList;

import CommonUtilityPackage.Excel_Data_Reader;

public class Post_RequestRepository extends Endpoints {

	public static String Post_TC1_Request() throws IOException {

		ArrayList<String> Excel_Data = Excel_Data_Reader.Read_Excel_data("API_Data.xlsx", "Post_API", "Post_TC_2");
		// System.out.println(Excel_Data);

		String Req_name = Excel_Data.get(1);
		String Req_job = Excel_Data.get(2);

		String requestbody = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"leader\"\r\n" + "}";

		return requestbody;
	}
}
