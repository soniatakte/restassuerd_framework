# RestAssuerd_Framework

This framework is designed for API testing using JAVA & RestAssured. Basically it focusses on sending HTTP requests,validating responses and handling various scenarios in dynamic and reusable way.
The project is structured as a Maven project, simplifying dependency management for the RestAssured framework. The 'pom.xml' file is configured with essential dependencies like RestAssured, TestNG, and Apache, facilitating the creation and execution of the framework.

In the 'src' package, the code is neatly organized into packages and classes. The 'Request Repository' package contains the 'Endpoints' class, which declares the base URI and resources for different APIs. The 'RequestRepository' classes extend 'RequestBody,' while the 'CommonMethod' package features the 'Trigger_API_Method' class, providing methods to trigger APIs, extract status codes, and response bodies.
The 'TestClass' package defines test cases for various APIs, encompassing logic for specific test case validations. The 'DriverClass' determines which tests to run by invoking the executor method of the test class. The 'Handle_API_Logs' stores test data, and 'ExcelDataReader' extracts data from Excel files.
this framework simplifies and organizes API testing, ensuring a systematic approach for efficient testing and validation.







